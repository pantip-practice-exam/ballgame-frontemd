export const showModelPredict = item => dispatch => {

    dispatch({ 
        type:  'MODEL_PREDICT_SHOW',
        payload: item
    })

}

export const predictModelPredict = () => dispatch => {

    dispatch({
        type: 'MODEL_PREDICT_PREDICT',
        status : false
    })

}

export const closeModelPredict = () => dispatch => {

    dispatch({
        type: 'MODEL_PREDICT_CLOSE',
        status : false
    })

}

