// Utils
import { createActionSet } from '../utilities/'

// Libs
import axios from 'axios'

const FETCH_MATCHES = createActionSet('FETCH_MATCHES')

export const fetchApiMatches = () => async dispatch => {

    dispatch({ type: FETCH_MATCHES.PENDING })
    
    try {
        
        const matchesData = await axios.get('http://localhost:5000/matches/3551123')

        dispatch({
            type: FETCH_MATCHES.SUCCESS,
            payload: matchesData.data
        })

    }
    catch (error) {

        dispatch({
            type: FETCH_MATCHES.FAILED,
            error
        })

    }

}


