import React from 'react'

const ListLastMatches = props => {

    return (
        <div className="box-mactch-result">
            <div className="box-title">
                <div className="head">ตารางการแข่งขัน</div>
                <div><a href="#!">ดูทั้งหมด</a></div>
            </div>
            <div className="box-result">
                <div className="box-macth-info">
                    <p> 20 December 2020</p>
                </div>
                <div className="box-macth-team">
                    <div className="team-list">
                        <img src="https://dummyimage.com/63x63/18ADBA/FAFAFA" alt="" />
                        <div className="team-list-label">FRANCE</div>
                    </div>
                    <div className="box-macth-score">
                        <span>3 - 1</span>
                    </div>
                    <div className="team-list">
                        <img src="https://dummyimage.com/63x63/18ADBA/FAFAFA" alt="" />
                        <div className="team-list-label">ENGLAND</div>
                    </div>
                </div>
                <div className="box-macth-info">
                    <p> 20 December 2020</p>
                </div>
            </div>
            <div className="box-result">
                <div className="box-macth-info">
                    <p> 20 December 2020</p>
                </div>
                <div className="box-macth-team">
                    <div className="team-list">
                        <img src="images/Group 603.png" alt="" />
                        <div className="team-list-label">FRANCE</div>
                    </div>
                    <div className="box-macth-score">
                        <span>3 - 1</span>
                    </div>
                    <div className="team-list">
                        <img src="images/Group 604.png" alt="" />
                        <div className="team-list-label">ENGLAND</div>
                    </div>
                </div>
                <div className="box-macth-info">
                    <p> 20 December 2020</p>
                </div>
            </div>
            <div className="box-result">
                <div className="box-macth-info">
                    <p> 20 December 2020</p>
                </div>
                <div className="box-macth-team">
                    <div className="team-list">
                        <img src="images/Group 603.png" alt="" />
                        <div className="team-list-label">FRANCE</div>
                    </div>
                    <div className="box-macth-score">
                        <span>3 - 1</span>
                    </div>
                    <div className="team-list">
                        <img src="images/Group 604.png" alt="" />
                        <div className="team-list-label">ENGLAND</div>
                    </div>
                </div>
                <div className="box-macth-info">
                    <p> 20 December 2020</p>
                </div>
            </div>
        </div>
    )
}

export default ListLastMatches