// Libs
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

// Actions 
import { fetchApiMatches } from '../../actions/fetchApi'
import { showModelPredict } from '../../actions/modalPredict'

const ListMatches = props => {

    const dispatch = useDispatch()

    const matchesData = useSelector(state => state.fetchApi.data)

    useEffect(() => {

        dispatch(fetchApiMatches())

    }, [dispatch])

    const randerListMatches = matchesData => {

        const { data: arrMatchesData } = matchesData

        const arrMatchesPredict = arrMatchesData.filter(item => item.predict_on_time)

        return arrMatchesPredict.map((item, key) => {

            const matchStartDate = new Date(item.match_start)

            const months = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.']
        
            let hours = `${matchStartDate.getHours()}`

            hours = hours.length < 2 ? `0${hours}` : hours            

            let minutes = `${matchStartDate.getMinutes()}`

            minutes = minutes.length < 2 ? `0${minutes}` : minutes

            const fullFormatDate = `${matchStartDate.getDate()} ${months[matchStartDate.getMonth()]} ${hours}:${minutes}`
        
            return (
                <div key={key} className="table-row row-content">
                    <div className="table-colunm-time">{fullFormatDate}</div>
                    <div className="table-colunm-team">
                        <span className="team-label">{item.team_a_th}</span>
                        <img src={`http:${item.team_a_image}`} alt={item.team_a_th} />
                        <span>VS</span>
                        <img src={`http:${item.team_b_image}`} alt={item.team_b_th} />
                        <span className="team-label">{item.team_b_th}</span>
                    </div>
                    <div className='table-colunm-fortune'>
                        {
                            item.is_show_edit ?
                                (<div className='button-macth btn-cancel' onClick={() => dispatch(showModelPredict(item))}>แก้ไข</div>)
                                :
                                (<div className='button-macth btn-fortune' onClick={() => dispatch(showModelPredict(item))}>ทายผล</div>)
                        }
                    </div>
                </div>
            )

        })

    }

    return (
        <div className="box-macth-table">
            <div className="box-title">
                <div>ตารางการแข่งขัน</div>
                <div><a href="#!">ดูทั้งหมด</a></div>
            </div>
            <div className="box-table">
                <div className="table-row row-header">
                    <div className="table-colunm-time">
                        เวลาแข่งขัน
            </div>
                    <div className="table-colunm-team">
                        ทีม
            </div>
                    <div className="table-colunm-fortune">
                        การทายผล
            </div>
                </div>

                {
                    (matchesData?.data && matchesData.data.length > 0) && randerListMatches(matchesData)
                }

            </div>
        </div>
    )

}

export default ListMatches