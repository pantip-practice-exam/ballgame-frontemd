// Libs 
import React from 'react'

const TablePredictTrue = props => {

    return (
        <div className="box-ranking-table ranking-fortune">
            <div className="box-title">
                <div>อันดับผู้ที่ทายถูกมากที่สุด</div>
                <div><a href="#!">ดูทั้งหมด</a></div>
            </div>
            <div className="box-table">
                <div className="table-row row-header">
                    <div className="table-colunm-rank">
                        ลำดับ
            </div>
                    <div className="table-colunm-name">
                        ชื่อ
            </div>
                    <div className="table-colunm-credit">
                        เครดิต
            </div>
                </div>
                <div className="table-row row-content">
                    <div className="table-colunm-ranknumber">
                        1
            </div>
                    <div className="table-colunm-user">
                        สมาชิกหมายเลข 1499976
            </div>
                    <div className="table-colunm-credit">
                        42
            </div>
                </div>
                <div className="table-row row-content">
                    <div className="table-colunm-ranknumber">
                        2
            </div>
                    <div className="table-colunm-user">
                        littleOlaF
            </div>
                    <div className="table-colunm-credit">
                        41
            </div>
                </div>
                <div className="table-row row-content">
                    <div className="table-colunm-ranknumber">
                        3
            </div>
                    <div className="table-colunm-user">
                        สมาชิกหมายเลข 1499976
            </div>
                    <div className="table-colunm-credit">
                        41
            </div>
                </div>
                <div className="table-row row-content">
                    <div className="table-colunm-ranknumber">
                        4
            </div>
                    <div className="table-colunm-user">
                        megalaima
            </div>
                    <div className="table-colunm-credit">
                        40
            </div>
                </div>
                <div className="table-row row-content">
                    <div className="table-colunm-ranknumber">
                        5
            </div>
                    <div className="table-colunm-user">
                        สมาชิกหมายเลข 3247397
            </div>
                    <div className="table-colunm-credit">
                        40
            </div>
                </div>
                <div className="table-row row-content">
                    <div className="table-colunm-ranknumber">
                        6
            </div>
                    <div className="table-colunm-user">
                        สมาชิกหมายเลข 1497797
            </div>
                    <div className="table-colunm-credit">
                        39
            </div>
                </div>
                <div className="table-row row-content">
                    <div className="table-colunm-ranknumber">
                        7
            </div>
                    <div className="table-colunm-user">
                        สมาชิกหมายเลข 3998051
            </div>
                    <div className="table-colunm-credit">
                        39
            </div>
                </div>
                <div className="table-row row-content">
                    <div className="table-colunm-ranknumber">
                        8
            </div>
                    <div className="table-colunm-user">
                        เล่าขลุ่ย
            </div>
                    <div className="table-colunm-credit">
                        37
            </div>
                </div>
                <div className="table-row row-content">
                    <div className="table-colunm-ranknumber">
                        9
            </div>
                    <div className="table-colunm-user">
                        นายยุทธ
            </div>
                    <div className="table-colunm-credit">
                        36
            </div>
                </div>
                <div className="table-row row-content">
                    <div className="table-colunm-ranknumber">
                        10
            </div>
                    <div className="table-colunm-user">
                        สมาชิกหมายเลข 761352
            </div>
                    <div className="table-colunm-credit">
                        36
            </div>
                </div>
            </div>
        </div>
    )
}

export default TablePredictTrue