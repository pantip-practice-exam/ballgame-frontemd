import React from 'react'

const HeadarMenu = props => {

    return (
        <div className='menu-main'>

            <ul className='menu-main-list'>
                <li><a href='#!'>หน้าแรก</a></li>
                <li><a href='#!'>กติกา</a></li>
                <li><a href='#!'>ตารางแข็งขัน</a></li>
                <li><a href='#!'>อันดับทายผล</a></li>
                <li><a href='#!'>พูดคุย</a></li>
                <li><a href='#!'>เข้าสู่ระบบ</a></li>
            </ul>

            <div className='menu-user-profile'>
                <div className='menu-user-profile-image'>
                    <div className='profile-image'></div>
                </div>

                <div className='menu-user-profile-info'>
                    <a href='#!'>
                        <div className='menu-user-name'>สมาชิกหมายเลข 3551123</div>
                    </a>
                    <div className='menu-user-rank'>อันดับ: 3000</div>
                    <div className='menu-user-credit'>เครดิต: 1,000,000</div>
                </div>
            </div>

            <div className='menu-main-mobilebtn'>
                <a href='#!'>
                    <i className='material-icons'>menu</i>
                </a>
            </div>
            
        </div>
    )
}

export default HeadarMenu