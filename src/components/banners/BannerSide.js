// Libs
import React from 'react'

const BannerSide = props => {

    return (
        <div className='banner-side'>
            <div className='banner-leftside'></div>
            <div className='banner-rightside'></div>
        </div>
    )
}

export default BannerSide