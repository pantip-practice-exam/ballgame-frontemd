// Libs
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import axios from 'axios'

// Actions
import { closeModelPredict, predictModelPredict } from '../../actions/modalPredict'

const ModalPredict = props => {

    const [inputPredict, setInputPredict] = useState('')

    const [radioSelect, setRadioSelect] = useState('')

    const dispatch = useDispatch()

    const modelPredictStoreSate = useSelector(state => state.modalPredict)

    if (!modelPredictStoreSate.status) return false

    const item = modelPredictStoreSate.data

    const matchStartDate = new Date(item.match_start)

    const months = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.']

    const fullFormatDate = `${matchStartDate.getDate()} ${months[matchStartDate.getMonth()]} ${matchStartDate.getFullYear() + 543}`

    let hours = `${matchStartDate.getHours()}`

    hours = hours.length < 2 ? `0${hours}` : hours

    let minutes = `${matchStartDate.getMinutes()}`

    minutes = minutes.length < 2 ? `0${minutes}` : minutes

    const time = `${hours}:${minutes}`

    const handleInput = e => {

        const inputPredictValue = e.target.value

        if (inputPredict.length === 1 && inputPredictValue === '') setInputPredict('')

        if (Number(inputPredictValue)) setInputPredict(inputPredictValue)

    }

    const onClickPredict = async () => {

        const body = {
            menber_id: 3551123,
            match_id: item.match_id,
            predict: radioSelect,
            credit: inputPredict
        }

        await axios.post('http://localhost:5000/upsert_user_predict', body)

        dispatch(predictModelPredict())

    }

    return (
        <div>
            <div className="p-modal" />
            <div className="dialog-macth dialog-fortune">
                <div className="dialog-header">
                    <div>{`คู่วันที่ ${fullFormatDate} เวลา ${time}`}</div>
                    <a className='dialog-close' onClick={() => dispatch(closeModelPredict())}>
                        <i className='material-icons'>cancel</i>
                    </a>
                </div>
                <div className="dialog-macth-team">
                    <div className="team-list">
                        <img src={`http:${item.team_a_image}`} alt={item.team_a_th} />
                    </div>
                    <div className="dialog-macth-vs">
                        <p>VS</p>
                    </div>
                    <div className="team-list">
                        <img src={`http:${item.team_b_image}`} alt={item.team_b_th} />
                    </div>
                </div>
                <div className="dialog-choice">
                    <div className="choice">
                        <input id="a-win" type="radio" name="winfortune" onClick={() => setRadioSelect(item.team_a_en)} />
                        <label htmlFor='a-win'>{item.team_a_en}</label>
                    </div>
                    <div className="choice">
                        <input id="draw" type="radio" name="winfortune" onClick={() => setRadioSelect('Draw')} />
                        <label htmlFor="draw">Draw</label>
                    </div>
                    <div className="choice">
                        <input id="b-win" type="radio" name="winfortune" onClick={() => setRadioSelect(item.team_b_en)} />
                        <label htmlFor='b-win'>{item.team_b_en}</label>
                    </div>
                </div>
                <div className="dialog-header sub-header">
                    <div>จำนวนคนทายผล</div>
                </div>
                <div className="dialog-macth-point">
                    <div className="dialog-point">
                        <p className="point">{`?`}</p>
                    </div>
                    <div className="dialog-point point-draw">
                        <p className="point">{`?`}</p>
                    </div>
                    <div className="dialog-point">
                        <p className="point">{`?`}</p>
                    </div>
                </div>
                <div className="dialog-header sub-header">
                    <div>จำนวนเครดิต</div>
                </div>
                <div className="dialog-macth-point">
                    <div className="dialog-point">
                        <p className="point">{`?`}</p>
                    </div>
                    <div className="dialog-point point-draw">
                        <p className="point">{`?`}</p>
                    </div>
                    <div className="dialog-point">
                        <p className="point">{`?`}</p>
                    </div>
                </div>
                <div className="dialog-header sub-header">
                    <div>ใส่จำนวนเครดิต</div>
                </div>
                <div className='dialog-credit'>
                    <input type='text' value={inputPredict} onChange={handleInput} />

                    <a href='/' onClick={onClickPredict}>
                        <div className='button-macth btn-fortune' >ทายผล</div>
                    </a>

                </div>
            </div>
        </div>
    )

}

export default ModalPredict