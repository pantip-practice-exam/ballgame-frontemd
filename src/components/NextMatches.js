// Utils
import { formatAMPM } from '../utilities'

// Libs
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

// Actions
import { fetchApiMatches } from '../actions/fetchApi'
import { showModelPredict } from '../actions/modalPredict'

const NextMatches = props => {

    const { limitShowBox = 3 } = props

    const dispatch = useDispatch()

    const matchesData = useSelector(state => state.fetchApi.data)

    useEffect(() => {

        dispatch(fetchApiMatches())

    }, [dispatch])

    const randerBoxMacth = matchesData => {

        const { data: arrMatchesData } = matchesData

        const arrMatchesPredict = arrMatchesData.filter(item => item.predict_on_time).slice(0, limitShowBox)

        return arrMatchesPredict.map((item, key) => {

            const matchStartDate = new Date(item.match_start)

            const months = ['January', 'February ', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

            const fullFormatDate = `${matchStartDate.getDate()} ${months[matchStartDate.getMonth()]} ${matchStartDate.getFullYear()}`

            const time = formatAMPM(matchStartDate)

            return (
                <div key={key} className='box-macth'>
                    <div className='box-header'>
                        <p>Next Macth</p>
                    </div>
                    <div className='box-macth-next'>
                        <div className="box-macth-team">
                            <div className="team-list">
                                <img src={`http:${item.team_a_image}`} alt={item.team_a_th} />
                                <div className='team-list-label'>{item.team_a_en}</div>
                            </div>
                            <div className='box-macth-vs'>
                                <p>VS</p>
                            </div>
                            <div className='team-list'>
                                <img src={`http:${item.team_b_image}`} alt={item.team_b_th} />
                                <div className='team-list-label'>{item.team_b_en}</div>
                            </div>
                        </div>
                    </div>
                    <div className='box-macth-info'>
                        <p>{fullFormatDate}</p>
                        <p>{time}</p>
                        <p>Stadium {`?`}</p>
                    </div>
                    <div className='box-macth-point'>
                        <div className='box-point'>
                            <p className='point'>{`?`}</p>
                            <p className="point-label">{item.team_a_en}</p>
                        </div>
                        <div className="box-point point-draw">
                            <p className="point">{`?`}</p>
                            <p className="point-label">Draw</p>
                        </div>
                        <div className='box-point'>
                            <p className="point">{`?`}</p>
                            <p className="point-label">{item.team_b_en}</p>
                        </div>
                    </div>
                    <div className='box-macth-fortune'>
                        {
                            item.is_show_edit ?
                                (<div className='button-macth btn-cancel' onClick={() => dispatch(showModelPredict(item))}>แก้ไข</div>)
                                :
                                (<div className='button-macth btn-fortune' onClick={() => dispatch(showModelPredict(item))}>ทายผล</div>)
                        }
                    </div>
                </div>
            )
        })

        // return (
        //     <div className="box-macth">
        //         <div className="box-header">
        //             <p>Next Macth</p>
        //         </div>
        //         <div className="box-macth-next">
        //             <div className="box-macth-team">
        //                 <div className="team-list">
        //                     <img src="https://dummyimage.com/77x77/18ADBA/FAFAFA" alt="" />
        //                     <div className="team-list-label">Czech Republic</div>
        //                 </div>
        //                 <div className="box-macth-vs">
        //                     <p>VS</p>
        //                 </div>
        //                 <div className="team-list">
        //                     <img src="https://dummyimage.com/77x77/18ADBA/FAFAFA" alt="" />
        //                     <div className="team-list-label">Natherland</div>
        //                 </div>
        //             </div>
        //         </div>
        //         <div className="box-macth-info">
        //             <p> 20 December 2020</p>
        //             <p> 04:00 PM</p>
        //             <p> Stadium NYK</p>
        //         </div>
        //         <div className="box-macth-point">
        //             <div className="box-point">
        //                 <p className="point">100,000</p>
        //                 <p className="point-label">Czech Republic</p>
        //             </div>
        //             <div className="box-point point-draw">
        //                 <p className="point">100,000</p>
        //                 <p className="point-label">Draw</p>
        //             </div>
        //             <div className="box-point">
        //                 <p className="point">100,000</p>
        //                 <p className="point-label">Natherland</p>
        //             </div>
        //         </div>
        //         <div className="box-macth-fortune">
        //             <a href="#!"><div className="button-macth btn-fortune">
        //                 ทายผล
        //         </div></a>
        //         </div>
        //     </div>
        // )
    }

    return (
        <div className='box-next-macth'>
            <div className="box-macth-slide-container">
                <div className="box-macth-slide">
                    {
                        (matchesData?.data && matchesData.data.length > 0) && randerBoxMacth(matchesData)
                    }
                </div>
            </div>
        </div>
    )
}

export default NextMatches