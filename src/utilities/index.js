// For Create Actions Type
export const createActionSet = actionName => ({
    PENDING: `${actionName}_PENDING`,
    SUCCESS: `${actionName}_SUCCESS`,
    FAILED: `${actionName}_FAILED`
})


export const formatAMPM = (date) => {

    let hours = date.getHours()
    let minutes = date.getMinutes()

    let ampm = hours >= 12 ? 'PM' : 'AM'

    hours = hours % 12
    hours = hours ? hours : 12; // the hour '0' should be '12'

    minutes = minutes < 10 ? '0' + minutes : minutes
    
    hours = `${hours}`

    hours = hours.length < 2 ? `0${hours}` : hours

    const strTime = `${hours}:${minutes} ${ampm}`

    return strTime

}