// Libs
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'

// Reducers
import reducers from '../reducers'

const createReduxStore = (initialState, options) => {
    const store = createStore(
        reducers,
        applyMiddleware(thunk, createLogger)
    )

    return store
}

export default createReduxStore