// Libs
import { combineReducers } from 'redux'

// reducers
import fetchApiReducer from './fetchApiReducer'
import modalPredictReducer from './modalPredictReducer'

export default combineReducers({
    fetchApi : fetchApiReducer,
    modalPredict : modalPredictReducer
})
