// Utils
import { createActionSet } from '../utilities'

const FETCH_MATCHES = createActionSet('FETCH_MATCHES')

const initialState = {
    status: false,
    data: {}
}

export default (state = initialState, dispatch) => {

    const { type, payload } = dispatch

    switch (type) {
        
        // Pending
        case 'MODEL_PREDICT_SHOW':
            return {
                ...state,
                status: true,
                data : payload
            }
        
        // Success
        case 'MODEL_PREDICT_PREDICT':
            return {
                ...state,
                status: false,
                data : {}
            }

        // Failed
        case 'MODEL_PREDICT_CLOSE':
            return {
                ...state,
                status: false,
                data : {}
            }

        default:
            return state
    }
}

