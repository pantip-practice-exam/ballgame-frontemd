// Utils
import { createActionSet } from '../utilities'

const FETCH_MATCHES = createActionSet('FETCH_MATCHES')

const initialState = {
    isFetching: false,
    error: null,
    data: {}
}

export default (state = initialState, dispatch) => {

    const { type, payload, error } = dispatch

    switch (type) {
        
        // Pending
        case FETCH_MATCHES.PENDING:
            return {
                ...state,
                isFetching: true
            }
        
        // Success
        case FETCH_MATCHES.SUCCESS:
            return {
                ...state,
                isFetching: false,
                data: payload
            }

        // Failed
        case FETCH_MATCHES.FAILED:
            return {
                ...state,
                isFetching: false,
                error
            }

        default:
            return state
    }
}

