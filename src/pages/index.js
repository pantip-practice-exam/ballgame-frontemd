// Libs
import React from 'react'

// Components 
import ModelPredict from '../components/modal/ModalPredict'

import BannerTop from '../components/banners/BannerTop'
import BannerSide from '../components/banners/BannerSide'
import BannerMain from '../components/banners/BannerMain'

import HeadarMenu from '../components/HeadarMenu'
import NextMatches from '../components/NextMatches'

import ListMatches from '../components/list/ListMatches'
import ListTopicPickup from '../components/list/ListTopicPickup'

import ListListMatches from '../components/list/ListLastMatchres'
import TableCreditRanking from '../components/table/TableCreditRanking'
import TablePredictTrue from '../components/table/TablePredictTrue'

import Footer from '../components/Footer'

const Index = props => {

    return (
       <React.Fragment>
          
          <ModelPredict />
          
           <div className='container'>

             <BannerTop />

             <BannerSide />

             <HeadarMenu />

             <BannerMain />

             <NextMatches />

             <div className='row'>

               <div className='box-main-content col-xl-8'>
                  
                  <ListMatches />

                  <ListTopicPickup />
                
               </div>

               <div className='box-side-content col-xl-4'>

                  <ListListMatches />

                  <TableCreditRanking />

                  <TablePredictTrue />

               </div>

             </div>

           </div>

           <Footer />

       </React.Fragment>
    )

}

export default Index