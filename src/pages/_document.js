// Libs
import React from 'react'
import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {

    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)
        return { ...initialProps }
    }

    render() {
        return (
            <Html>
                <Head>
                    <meta charSet='utf-8' />
                    <meta name='viewport' content='width=device, initial-scale=1, shrink-to-fit=no' />

                    <link rel='stylesheet' href='./static/css/main.css' />
                </Head>
                
                <body>
                        <Main />
                        <NextScript />
                </body>
            </Html>
        )
    }
}

export default MyDocument